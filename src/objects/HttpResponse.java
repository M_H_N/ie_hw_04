package objects;

import java.io.*;
import java.net.Socket;
import java.util.Optional;
import java.util.function.Consumer;

public class HttpResponse {
    private Optional<HttpHeader> header = Optional.empty();
    private Optional<HttpBody> body = Optional.empty();

    public HttpResponse send(Socket client) {
        try {
            this.send(client.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }

    public HttpResponse send(OutputStream outputStream) {
        this.body.flatMap(HttpBody::getFileData).ifPresent(file -> {
            try (BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream))) {
                this.header.map(HttpHeader::getHeaderString).ifPresent(s -> {
                    try {
                        bufferedWriter.write(s, 0, s.length());
                        bufferedWriter.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
                    char[] buffer = new char[4096];
                    int read;
                    do {
                        read = bufferedReader.read(buffer, 0, buffer.length);
                        if (read <= 0) continue;
                        bufferedWriter.write(buffer, 0, read);
                        bufferedWriter.flush();
//                        bufferedWriter.flush();
                    } while (read > 0);
                } catch (Exception e) {
//                    e.printStackTrace();
                }
            } catch (Exception e) {
//                e.printStackTrace();
            }
        });
        return this;
    }

    public Optional<HttpHeader> getHeader() {
        return header;
    }

    public HttpResponse setHeader(Optional<HttpHeader> header) {
        this.header = header;
        return this;
    }

    public Optional<HttpBody> getBody() {
        return body;
    }

    public HttpResponse setBody(Optional<HttpBody> body) {
        this.body = body;
        return this;
    }
}
