package objects;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;

public class FileManager {
    private static final File MAIN_DIRECTORY = new File("myserver");

    static {
        if (!MAIN_DIRECTORY.exists()) MAIN_DIRECTORY.mkdirs();
    }

    public static Optional<File> getFileWithRelativePath(String relativePath) {
        System.out.println("getFileWithRelativePath: flag-00:\n" + relativePath);
        try {
            File file = new File(MAIN_DIRECTORY, relativePath);
            if (!file.exists() || file.isDirectory()) return Optional.empty();
            return Optional.of(file);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public static String getContentTypeForFile(File file) {
        try {
            return Files.probeContentType(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
            return "*/*";
        }
    }

}
