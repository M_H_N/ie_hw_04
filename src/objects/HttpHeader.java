package objects;

import enums.HttpConnectionType;
import enums.HttpMethod;
import enums.HttpResponseType;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Optional;
import java.util.function.Consumer;

public class HttpHeader {
    private Optional<HttpMethod> method = Optional.empty();
    private Optional<String> relativePath = Optional.empty();
    private Optional<String> httpVersion = Optional.empty();

    private Optional<String> contentType = Optional.empty();
    private Optional<String> contentLength = Optional.empty();
    private Optional<HttpResponseType> responseType = Optional.empty();

    private Optional<String> userAgent = Optional.empty();
    private Optional<String> acceptLanguage = Optional.empty();
    private Optional<String> acceptEncoding = Optional.empty();
    private Optional<String> acceptCharset = Optional.empty();
    private Optional<Long> keepAliveMS = Optional.empty();
    private Optional<HttpConnectionType> connectionType = Optional.empty();
    private Dictionary<String, String> customHeaders = new Hashtable<>();

    public HttpHeader fill(String headerLine) {
        if (!headerLine.contains(" ")) return this;
        String[] split = headerLine.split(" ");
        if (split.length < 2) return this;
        if (split.length == 3 && this.relativePath.isEmpty()) {
            this.method = HttpMethod.parse(split[0]);
            this.relativePath = Optional.of(split[1]);
            this.httpVersion = Optional.of(split[2]);
        } else {
            switch (split[0].toUpperCase()) {
                case "USER-AGENT:":
                    this.userAgent = Optional.of(split[1]);
                    break;
                case "ACCEPT-LANGUAGE:":
                    this.acceptLanguage = Optional.of(split[1]);
                    break;
                case "ACCEPT-ENCODING:":
                    this.acceptEncoding = Optional.of(split[1]);
                    break;
                case "ACCEPT-CHARSET:":
                    this.acceptCharset = Optional.of(split[1]);
                    break;
                case "CONNECTION:":
                    this.connectionType = HttpConnectionType.parse(split[1]);
                    break;
                case "CONTENT-TYPE:":
                    this.contentType = Optional.of(split[1]);
                    break;
                default:
                    this.customHeaders.put(split[0], split[1]);
                    break;
            }
        }
        return this;
    }

    public String getHeaderString() {
        StringBuilder builder = new StringBuilder("HTTP/1.1");
        this.responseType.ifPresent(httpResponseType -> builder.append(" ").append(httpResponseType.getRawCodeValue()).append(" ").append(httpResponseType.getRawStringValue()));
        this.contentLength.ifPresent(s -> builder.append("\nContent-Length: ").append(s));
        this.contentType.ifPresent(s -> builder.append("\nContent-Type: ").append(s));
        this.connectionType.ifPresent(httpConnectionType -> builder.append("\nConnection: ").append(httpConnectionType.getRawValue()));
        return builder.append("\n\n").toString();
    }

    public Optional<HttpMethod> getMethod() {
        return method;
    }

    public HttpHeader setMethod(Optional<HttpMethod> method) {
        this.method = method;
        return this;
    }

    public Optional<String> getRelativePath() {
        return relativePath;
    }

    public HttpHeader setRelativePath(Optional<String> relativePath) {
        this.relativePath = relativePath;
        return this;
    }

    public Optional<String> getHttpVersion() {
        return httpVersion;
    }

    public HttpHeader setHttpVersion(Optional<String> httpVersion) {
        this.httpVersion = httpVersion;
        return this;
    }

    public Optional<String> getUserAgent() {
        return userAgent;
    }

    public HttpHeader setUserAgent(Optional<String> userAgent) {
        this.userAgent = userAgent;
        return this;
    }

    public Optional<String> getAcceptLanguage() {
        return acceptLanguage;
    }

    public HttpHeader setAcceptLanguage(Optional<String> acceptLanguage) {
        this.acceptLanguage = acceptLanguage;
        return this;
    }

    public Optional<String> getAcceptEncoding() {
        return acceptEncoding;
    }

    public HttpHeader setAcceptEncoding(Optional<String> acceptEncoding) {
        this.acceptEncoding = acceptEncoding;
        return this;
    }

    public Optional<String> getAcceptCharset() {
        return acceptCharset;
    }

    public HttpHeader setAcceptCharset(Optional<String> acceptCharset) {
        this.acceptCharset = acceptCharset;
        return this;
    }

    public Optional<Long> getKeepAliveMS() {
        return keepAliveMS;
    }

    public HttpHeader setKeepAliveMS(Optional<Long> keepAliveMS) {
        this.keepAliveMS = keepAliveMS;
        return this;
    }

    public Optional<HttpConnectionType> getConnectionType() {
        return connectionType;
    }

    public HttpHeader setConnectionType(Optional<HttpConnectionType> connectionType) {
        this.connectionType = connectionType;
        return this;
    }

    public Dictionary<String, String> getCustomHeaders() {
        return customHeaders;
    }

    public HttpHeader setCustomHeaders(Dictionary<String, String> customHeaders) {
        this.customHeaders = customHeaders;
        return this;
    }

    public Optional<String> getContentType() {
        return contentType;
    }

    public HttpHeader setContentType(Optional<String> contentType) {
        this.contentType = contentType;
        return this;
    }

    public Optional<String> getContentLength() {
        return contentLength;
    }

    public HttpHeader setContentLength(Optional<String> contentLength) {
        this.contentLength = contentLength;
        return this;
    }

    public Optional<HttpResponseType> getResponseType() {
        return responseType;
    }

    public HttpHeader setResponseType(Optional<HttpResponseType> responseType) {
        this.responseType = responseType;
        return this;
    }
}
