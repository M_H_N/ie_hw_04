package objects;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;

public class HttpRequest {
    private Optional<HttpHeader> header = Optional.empty();
    private Optional<HttpBody> body = Optional.empty();


    public static Optional<HttpRequest> parse(InputStream inputStream) {
        try {
            HttpRequest request = new HttpRequest();
            HttpHeader header = new HttpHeader();
            HttpBody body = new HttpBody();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String inputLine;
            System.out.println("Reading Header");
            while (!(inputLine = bufferedReader.readLine()).equals("")) {
                System.out.println("Header:\n" + inputLine);
                header.fill(inputLine);
            }
            System.out.println("Reading Body");
            StringBuilder payload = new StringBuilder();
            while (bufferedReader.ready()) payload.append((char) bufferedReader.read());
//            System.out.println("Body:\n" + payload.toString());
            body.setStringData(Optional.of(payload.toString()));
            request.setHeader(Optional.of(header)).setBody(Optional.of(body));
            return Optional.of(request);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }


    public Optional<HttpHeader> getHeader() {
        return header;
    }

    public HttpRequest setHeader(Optional<HttpHeader> header) {
        this.header = header;
        return this;
    }

    public Optional<HttpBody> getBody() {
        return body;
    }

    public HttpRequest setBody(Optional<HttpBody> body) {
        this.body = body;
        return this;
    }
}
