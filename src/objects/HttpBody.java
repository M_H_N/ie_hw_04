package objects;

import java.io.File;
import java.util.Optional;
import java.util.function.Consumer;

public class HttpBody {
    private Optional<String> stringData = Optional.empty();
    private Optional<File> fileData = Optional.empty();

    public HttpBody fillStringData(String bodyLine) {
        this.stringData.ifPresentOrElse(s -> s += bodyLine, () -> this.stringData = Optional.of(bodyLine));
        return this;
    }

    public Optional<String> getStringData() {
        return stringData;
    }

    public HttpBody setStringData(Optional<String> stringData) {
        this.stringData = stringData;
        return this;
    }

    public Optional<File> getFileData() {
        return fileData;
    }

    public HttpBody setFileData(Optional<File> fileData) {
        this.fileData = fileData;
        return this;
    }
}
