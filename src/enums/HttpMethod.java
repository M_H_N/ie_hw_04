package enums;

import java.util.Optional;

public enum HttpMethod {
    GET("GET"), POST("POST");

    private String rawValue;

    HttpMethod(String rawValue) {
        this.rawValue = rawValue;
    }

    public static Optional<HttpMethod> parse(String rawValue) {
        switch (rawValue.toUpperCase()) {
            case "GET":
                return Optional.of(GET);
            case "POST":
                return Optional.of(POST);
            default:
                return Optional.empty();
        }
    }

    public String getRawValue() {
        return rawValue;
    }

    public HttpMethod setRawValue(String rawValue) {
        this.rawValue = rawValue;
        return this;
    }
}