package enums;

public enum HttpResponseType {
    OK(200, "OK"), NOT_FOUND(404, "Not Found");

    private int rawCodeValue;
    private String rawStringValue;

    HttpResponseType(int rawCodeValue, String rawStringValue) {
        this.rawCodeValue = rawCodeValue;
        this.rawStringValue = rawStringValue;
    }

    public int getRawCodeValue() {
        return rawCodeValue;
    }

    public HttpResponseType setRawCodeValue(int rawCodeValue) {
        this.rawCodeValue = rawCodeValue;
        return this;
    }

    public String getRawStringValue() {
        return rawStringValue;
    }

    public HttpResponseType setRawStringValue(String rawStringValue) {
        this.rawStringValue = rawStringValue;
        return this;
    }
}
