package enums;

import java.util.Optional;

public enum HttpConnectionType {
    KEEP_ALIVE("keep-alive"),
    CLOSE("close");

    private String rawValue;

    HttpConnectionType(String rawValue) {
        this.rawValue = rawValue;
    }

    public static Optional<HttpConnectionType> parse(String rawValue) {
        switch (rawValue.toUpperCase()) {
            case "KEEP-ALIVE":
                return Optional.of(KEEP_ALIVE);
            case "CLOSE":
                return Optional.of(CLOSE);
            default:
                return Optional.empty();
        }
    }

    public String getRawValue() {
        return rawValue;
    }

    public HttpConnectionType setRawValue(String rawValue) {
        this.rawValue = rawValue;
        return this;
    }


}
